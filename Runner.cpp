#include <iostream>
#include <stdlib.h>
using namespace std;

string genHuman(int &length);
string genOcto(int &length);
string genFreak(int &length);
string genString(int &a, int &g, int &c, int &t, int &length);

int main(){
int length = 0;

cout << "DNA Generator Program" << endl;
cout << endl;
cout << "How long should the DNA strand be?" << endl;
cin >> length;
cout << "This is Human's DNA sequence" << endl;
cout << genHuman(length) << endl;
cout << "29% A, 21% G, 20% C, 30% T" << endl;
cout << endl;
cout << "This is another Human's DNA sequence" << endl;
cout << genHuman(length) << endl;
cout << "29% A, 21% G, 20% C, 30% T" << endl;
cout << endl;
cout << "This is an Octopus' DNA sequence" << endl;
cout << genOcto(length) << endl;
cout << "33% A, 18% G, 18% C, 18% T" << endl;
cout << endl;
cout << "This abomination has equal parts A, G, C, and T" << endl;
cout << genFreak(length) << endl;
cout << "25% A, 25%G, 25% C, 25% T";

return 0;
}

int genRando(){ 
int rando = rand()%4; 
return rando;
}

string genString(int& a, int& g, int& c, int& t, int& length){
string sequence = "";
int remA = a;
int remG = g;
int remC = c;
int remT = t;
int x;
cout << "A: " << remA << " G: " << remG << " C: " << remC << " T: " << remT <<endl;
for(int i = 0; i < length; i++){
  x = genRando();
  if(x == 0){
    if(remA > 0){
      sequence += "A";
      remA--;
    }
    else x = 1;
  }
  if(x == 1){
    if(remG > 0){
      sequence += "G";
      remG--;
    }
    else x = 2;
  }
  if(x == 2){
    if(remC > 0){
      sequence += "C";
      remC--;
    }
    else x = 3;
  }
  if(x == 3){
    if(remT > 0){
      sequence += "T";
      remT--;
    }
    else x = 0;
  }
}
return sequence;
}

string genHuman(int& length){
int a = length*.29;
int g = length*.21;
int c = length*.2;
int t = length*.3;
return genString(a, g, c, t, length);
}

string genOcto(int& length){
int a = length*.33;
int g = length*.18;
int c = length*.18;
int t = length*.32;
return genString(a, g, c, t, length);
}

string genFreak(int& length){
int a = length*.25;
int g = length*.25;
int c = length*.25;
int t = length*.25;
return genString(a, g, c, t, length);
}
